
#!/usr/bin/python

# This code executes a search request for the specified search term.



from googleapiclient.discovery import build
from googleapiclient.errors import HttpError



DEVELOPER_KEY = 'AIzaSyAnWBo7kyFYA6sK25tjdjLmo38cDgu3UM8'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
searchTerm = 'انغام'
options={'q': searchTerm,'max_results':5}

def youtube_search(term, n):
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

  # Call the search.list method to retrieve results matching the specified
  # query term.
  search_response = youtube.search().list(
    q=term,
    part='id,snippet',
    maxResults=n
  ).execute()

  videos = []
  channels = []
  playlists = []

  # Add each result to the appropriate list, and then display the lists of
  # matching videos, channels, and playlists.
  for search_result in search_response.get('items', []):
    if search_result['id']['kind'] == 'youtube#video':
      videos.append(' (%s)' % (search_result['snippet']['title']))
    #elif search_result['id']['kind'] == 'youtube#channel':
      #channels.append('%s (%s)' % (search_result['snippet']['title'],
                                   #search_result['id']['channelId']))
    #elif search_result['id']['kind'] == 'youtube#playlist':
      #playlists.append('%s (%s)' % (search_result['snippet']['title'],
                                    #search_result['id']['playlistId']))
  results = '\n'.join(videos)
  print('Videos:\n', '\n'.join(videos), '\n')
  return results
  #print('Channels:\n', '\n'.join(channels), '\n')
  #print('Playlists:\n', '\n'.join(playlists), '\n')


def ySearch(searchTerm, res):
  try:
    songs = youtube_search(searchTerm, res)
    return songs
  except HttpError as e:
    print("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))
