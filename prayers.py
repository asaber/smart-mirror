import requests
import datetime
import time


def getPrayers():

    country = 'Egypt'
    city = 'cairo'
    api_call = 'http://api.aladhan.com/v1/timingsByCity?city=' + city + '&country=' + country + '&method=8'

    data = requests.get(api_call)
    data = data.json()
    bata = data['data']['timings']
    res = []
    if data['code'] == 200 :
        res.append('مواقيت الصلاة اليوم : ')
        res.append('صلاة الفجر الساعة {}'.format(bata['Fajr']) )
        res.append('صلاة الظهر الساعة {}'.format(bata['Dhuhr']))
        res.append('صلاة العصر الساعة {}'.format(bata['Asr']))
        res.append('صلاة المغرب الساعة {}'.format(bata['Maghrib']))
        res.append('صلاة العشاء الساعة {}'.format(bata['Isha']))

        print('\n'.join(res))
        return('\n'.join(res))
    else :
        return ('فشل في جلب مواقيت الصلاة')


