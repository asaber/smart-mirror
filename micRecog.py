import speech_recognition as sr
from datetime import datetime

def voiRec():
    r = sr.Recognizer()

    with sr.Microphone() as source :
        r.energy_threshold = 500
        r.dynamic_energy_threshold = False
        r.adjust_for_ambient_noise(source) 
        print('start talking')
        audio = r.listen(source, timeout = 60)
        print('end')


    try :
        res = r.recognize_google(audio, language='ar-EG')
        
        print("text is : ", res)
        return res
        
    except sr.UnknownValueError:
        err = "Google Speech Recognition could not understand audio"
        print(err)
        return err
    except sr.RequestError as e:
        nerr = "Could not request results from Google Speech Recognition service; {0}".format(e)
        print(nerr)
        return nerr