import tkinter

import youtubeSearch
from bidi import algorithm
import arabic_reshaper

import weather
import twitter
import tube
import youtubeSearch
import micRecog
import news
import mail
import prayers

from os import system

from pygame import mixer

from gtts import gTTS


top = tkinter.Tk()
top.title("Welcome to Smart Mirror")

lbl = tkinter.Label(top, text="", font=("ubuntu Arabic", 13))
lbl.grid(column=0, row=8)

mixer.init()
mixer.music.set_volume(1.0)
mixer.music.load("hello.mp3")
mixer.music.play()

def makeArabic(tx):
    tx = algorithm.get_display(tx)
    tx = arabic_reshaper.reshape(tx)
    return tx

def makeTextVoice(text, voiceFile) :
    tts = gTTS(text= text, lang='ar')
    tts.save(voiceFile)
    mixer.music.load(voiceFile)
    mixer.music.play()

def emailClicked():
    newMails = mail.fetchMail()
    newMails = '\n'.join(newMails)
    makeTextVoice(newMails, "email.mp3")

    
    lbl.configure(text= newMails, justify='left', font=("Arial Bold", 13) )
    lbl.grid(row=10, column=0, columnspan=3)


def weatherClicked():
    newText = weather.get_data()

    makeTextVoice(newText, "weather.mp3")

    newText = algorithm.get_display(newText)
    newText = arabic_reshaper.reshape(newText)

    lbl.configure(text= newText, justify='right', font=("ubuntu Arabic", 13))

def twitterClicked():
    Lb1 = tkinter.Listbox(top, width = 100, justify='right', font=("ubuntu Arabic", 11))
    recentTweets = twitter.recentTweets('@alaraby_ar')
    arr = []

    for num, tweet in enumerate(recentTweets, start=1) :
        char_list = [tweet[j] for j in range(len(tweet)) if ord(tweet[j]) in range(65536)]
        tweet=''
        for j in char_list:
            tweet=tweet+j
            
        arr.append(tweet)
        
        Lb1.insert( num, arabic_reshaper.reshape(algorithm.get_display(tweet) ) )
    
    arr = '\n'.join(arr)
    makeTextVoice(arr, "twitter.mp3")
    Lb1.grid(row=10, column=0, columnspan=3)

def tubeClicked():

    searchResult = youtubeSearch.ySearch('تنمية بشرية', 5)

    tts = gTTS(text= searchResult, lang='ar')
    tts.save("tube.mp3")
    mixer.music.load("tube.mp3")
    mixer.music.play()

def voiceText():
    #startTalking = "ابدا كلام"
    #startTalking = makeArabic(startTalking)
    #lbl.configure(text= startTalking, justify='right', font=("ubuntu Arabic", 13))

    voiceToText = micRecog.voiRec()
    voiceToTextArabic = makeArabic(voiceToText)
    #voiceToText = algorithm.get_display(voiceToText)
    #voiceToText = arabic_reshaper.reshape(voiceToText)
    if "ﻛﻮﺑﺴﻴﻒ" in voiceToTextArabic : print('facebook')
    elif "ﺑﻮﻳﺘﻮﻱ" in voiceToTextArabic : tubeClicked()
    elif "ﻫﺮﺍﺭﺣﻼ" in voiceToTextArabic : weatherClicked()
    elif "ﺭﺗﻴﻮﺕ" in voiceToTextArabic : twitterClicked()
    elif "ﺭﺍﺑﺨﺎﻻ" in voiceToTextArabic : getNews()
    print(voiceToTextArabic)
    #lbl.configure(text= voiceToTextArabic, justify='right', font=("ubuntu Arabic", 13))

def getNews():

    newsResult = news.fetchNews('مصر')
    newsResult = '\n'.join(newsResult)

    tts = gTTS(text= newsResult, lang='ar')
    tts.save("news.mp3")
    mixer.music.load("news.mp3")
    mixer.music.play()
    
    newsResult = makeArabic(newsResult)
    lbl.configure(text= newsResult, justify='right', font=("ubuntu Arabic", 13) )

def getPrayers():
    prayerTimes = prayers.getPrayers()
    makeTextVoice(prayerTimes, "prayers.mp3")



btn = tkinter.Button(top, text="Email", font=("Arial Bold", 25), background="#ffffff", 
width=15, command=emailClicked)
btn.grid(column=0, row=1)

btn2 = tkinter.Button(top, text="Youtube", font=("Arial Bold", 25), background="#cd201f", width=15,
command=tubeClicked )
btn2.grid(column=1, row=1)

btn3 = tkinter.Button(top, text="Twitter", font=("Arial Bold", 25), background="#55acee", width=15, 
command=twitterClicked )
btn3.grid(column=0, row=2)


btn4 = tkinter.Button(top, text="Weather", font=("Arial Bold", 25), width=15, command=weatherClicked)
btn4.grid(column=1, row=2)

btn5 = tkinter.Button(top, text="Voice Recog", font=("Arial Bold", 25), width=15, command=voiceText )
btn5.grid(column=0, row=3)

btn6 = tkinter.Button(top, text="Facebook", font=("Arial Bold", 25), background="#3b5999", width=15,
command=weatherClicked )
btn6.grid(column=1, row=3)

btn7 = tkinter.Button(top, text="News", font=("Arial Bold", 25), background="#cd201f", width=15,
command=getNews )
btn7.grid(column=0, row=4)

btn8 = tkinter.Button(top, text="prayerTimes", font=("Arial Bold", 25), background="#ef9b0f", width=15,
command=getPrayers )
btn8.grid(column=1, row=4)




top.geometry('700x600')
top.mainloop()