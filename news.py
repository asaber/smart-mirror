from newsapi import NewsApiClient

def fetchNews(keyword):
    newsapi = NewsApiClient(api_key='54cb7cd1ff354d8f901292e823d6872c')

    top_headlines = newsapi.get_top_headlines(q=keyword,language='ar')

    #print(top_headlines['articles'])

    articles = top_headlines['articles']

    res =[]

    for item in articles:
        for key,value in item.items():
            if(key == 'title'):
                res.append(value)
                
    return res